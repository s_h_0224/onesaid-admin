class API < Grape::API
  prefix 'api'
  version 'v1'
  format :json

  mount Endpoints::OSAuth
  mount Endpoints::OSUsers
  mount Endpoints::OSTopics
  mount Endpoints::OSCategories
  mount Endpoints::OSFriends
  mount Endpoints::OSFollowing
end
class Endpoints::OSAuth < Grape::API

	resources :auth do

	desc "Login a user"
    
    params do
      requires :email, 		          	type: String, desc: "zhon@admin.com"
      requires :password,             	type: String, desc: "User password"
    end
    post :login do
    	user = User.find_by_email(params[:email])

    	if user.present?
    		if user.valid_password?(params[:password])
                sign_in_count = user.sign_in_count + 1
                user.update_attributes(sign_in_count: sign_in_count, password: "")
                
                user_detail = user.api_detail(nil)
                data = {response: "success", status: "logined", message: "This user logined successfully", user_detail: user_detail}
    		else
    			data = {response: "fail", status: "login_failed", message: "Incorrect Password"}
    		end
    	else
    		data = {response: "fail", status: "login_failed", message: "Incorrect Email"}
    	end
    	data
    end
	end	
end
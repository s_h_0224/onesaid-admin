class Endpoints::OSCategories < Grape::API

	resources :categories do

    	desc "Get Categories List"
        
        params do
          
        end

        get do
        	categories = Category.all
            categories = categories.map{|category| category.api_detail}
            data = { response: "success", status: "get_categories", message: "Succeeded!", data: categories, count: categories.count}
        end
	end	
end
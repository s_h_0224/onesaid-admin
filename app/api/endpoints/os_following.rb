class Endpoints::OSFollowing < Grape::API
	
	resources :following do

		desc "Get All Users"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end

	    get :allusers do

	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		users = User.where.not(id: params[:user_id])
	    		# users = User.where("users.id<>'#{params[:user_id]}' and (users.first_name<>'' or users.last_name<>'')").all
	            res_users = users.map{|iuser| iuser.api_detail(user)}
	            data = { response: "success", status: "get_all_users", message: "Succeeded!", data: res_users, count: res_users.count}
	        else
	        	data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
	    	end
	    	
	    	data
	    end

		desc "Get All Following"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end	 

		get :following do
	    	curr_user = User.find(params[:user_id])
	    	if curr_user.present?
	    		users = curr_user.following
	    		following = users.map{|user| user.api_detail(curr_user)}
	    		data = { response: "success", status: "success_get_following", message: "Succeeded!", data: following, count: following.count }
	    	else
	    		data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
	    	end

	    	data
	    end

	    desc "Get All Followers"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end	 

		get :followers do
	    	curr_user = User.find(params[:user_id])
	    	if curr_user.present?
	    		users = curr_user.followers
	    		following = users.map{|user| user.api_detail(curr_user)}
	    		data = { response: "success", status: "success_get_following", message: "Succeeded!", data: following, count: following.count }
	    	else
	    		data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
	    	end

	    	data
	    end

	    desc "Follow"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    	requires :friend_user_id, 		type: Integer,  desc: "Friend User ID"
	    	requires :follow_status, 		type: Integer,  desc: "Status of follow and unfollow. 1 : follow , 0 : unfollow"
	    end

	    post :follow do
	    	if params[:user_id] == params[:friend_user_id]
	    		data = { response: "fail", status: "failed_follow", message: "Can't follow you" }
	    	else
	    		user = User.find_by_id(params[:user_id])
		    	if user.present?
		    		friend = User.find_by_id(params[:friend_user_id])
		    		if friend.present?
		    			if params[:follow_status] == 1
		    				user.follow!(friend)
		    				data = { response: "success", status: "follow_friend", message: "Succeeded!" }
		    			else
		    				user.unfollow!(friend)
		    				data = { response: "success", status: "unfollow_friend", message: "Succeeded!" }
		    			end
		    			
			    		
		    		else
		    			data = { response: "fail", status: "failed_find_friend", message: "Can't find this friend" }
		    		end
		    	else
		    		data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
		    	end
	    	end

	    	data
	    end
	end

end
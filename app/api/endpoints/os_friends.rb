class Endpoints::OSFriends < Grape::API
	
	resources :friends do

	    desc "Add New Friend"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    	requires :friend_user_id, 		type: Integer,  desc: "Friend User ID"
	    end

	    post :add_friend do
	    	if params[:user_id] == params[:friend_user_id]
	    		data = { response: "fail", status: "failed_add_friend", message: "Can't add you as a friend" }
	    	else
	    		user = User.find_by_id(params[:user_id])
		    	if user.present?
		    		friend = User.find_by_id(params[:friend_user_id])
		    		if friend.present?
		    			if user.add_friend(friend)
		    				data = { response: "success", status: "add_friend", message: "Succeeded!" }
		    			else
		    				data = { response: "fail", status: "failed_add_friend", message: "Can't add this friend" }
		    			end
		    		else
		    			data = { response: "fail", status: "failed_find_friend", message: "Can't find this friend" }
		    		end
		    	else
		    		data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
		    	end
	    	end

	    	data
	    end

	    desc "Get all friends"
	    params do
	    	requires :user_id, 						type: Integer, desc: "User ID"
	    end

	    get do
	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		friends = user.friends_list
	    		friends = friends.map{|friend| friend.api_detail(nil)}
	    		data = { response: "success", status: "success_get_all_friends", message: "Succeeded!", data: friends, count: friends.count }
	    	else
	    		data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
	    	end

	    	data
	    end
	end
end
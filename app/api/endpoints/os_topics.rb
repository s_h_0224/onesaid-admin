class Endpoints::OSTopics < Grape::API
	
	resources :topics do

	  	desc "Post a topic"
	    params do
	    	requires :name, 				type: String, desc: "About Cars"
	    	requires :desc, 				type: String, desc: "Topic description"
	    	requires :category_id, 			type: String, desc: "Parent Category ID"
	    	requires :user_id, 				type: String, desc: "User ID"
	    	requires :photo_uuid, 			type: String, desc: "Photo UUID"
	    end
	    
	    post :post_topic do
		    # user = User.where(first_name: params[:first_name], last_name: params[:last_name], specialty: params[:specialty]).first

		    p "REST API---------post_topic-------------"
		    p "params : #{params}"

		    user = User.find_by_id(params[:user_id])

		    if user.present?
		    	topic = user.topics.build(	name: 			params[:name],
		    					  			description: 	params[:desc],
		    					  			category_id: 	params[:category_id],
		    					  			photo_uuid: 	params[:photo_uuid])
		    	if topic.save
		    		data = {response: "success", status: "registered", message: "Topic posted successfully."}

		    		TopicPhoto.delay.transfer_and_cleanup(topic.id)
		    	else
		    		key, val = user.errors.messages.first
					data = {response: "fail", status: key, message: user.errors.full_messages.first}
		    	end
		    else
		    	data = {response: "fail", status: "user_not_exist", message: "This user not exist."}
		    end
		    data
	    end
  	end
end
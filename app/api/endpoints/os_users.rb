class Endpoints::OSUsers < Grape::API
	
	resources :users do
  	
	  	desc "Test user endpoints"
	  	post :test  do
	  		data = {data: params}
	  		data
	  	end

	  	desc "Get User's details"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end

	    get :user_details do
	    	user = User.find_by_id(params[:user_id])
	    	if params[:current_user_id].present?
	    		current_user = User.find_by_id(params[:current_user_id])
	    	else
	    		current_user = nil
	    	end
	    	if user.present?
	    		data = { response: "success", status: "get_user_details", message: "Succeeded!", user_detail: user.api_detail(current_user) }
	    	else
	    		data = { response: "fail", status: "no_exist_user", message: "Can't find this user", user_detail: "" }
	    	end

	    	data
	    end

	  	desc "Registers a user"
	    params do
	      requires :email,           		type: String, 	desc: "zhon@admin.com"
	      requires :username,           	type: String, 	desc: "zhon_doe"
	      # requires :last_name,            	type: String, desc: "Last name"
	      # requires :first_name,           	type: String, desc: "First name"
	      requires :gender, 				type: Integer, 	desc: "1: Male, 2: Female"
	      requires :birth, 					type: String, 	desc: "User Birthday"
	      requires :password,             	type: String, 	desc: "User password"
	      requires :user_role,             	type: Integer, 	desc: "User Role"
	      requires :user_type,             	type: Integer, 	desc: "User Type"
	    end
    
	    post :register do
		    # user = User.where(first_name: params[:first_name], last_name: params[:last_name], specialty: params[:specialty]).first
		    user = User.where("users.email='#{params[:email]}' or users.username='#{params[:username]}'").first
		    if user.present?
		    	data = {response: "fail", status: "duplicated", message: "This user already registered."}
		    else
		    	user = User.new(
		    		# first_name: 							params[:first_name], 
		    		# last_name: 								params[:last_name],
		    		email: 									params[:email],
		    		username: 								params[:username],
		    		password: 								params[:password],
		    		gender: 								params[:gender],
		    		birth: 									params[:birth],
		    		password_confirmation: 					params[:password],
		    		sign_in_count: 							1,
		    		user_role: 								params[:user_role],
		    		user_type: 								params[:user_type]
		    		)
		    	
		    	user.skip_confirmation!

		    	if user.save
		    		user_detail = user.api_detail(nil)
					data = {response: "success", status: "registered", message: "This user registered successfully.", user_detail: user_detail}
			    else
					key, val = user.errors.messages.first
					data = {response: "fail", status: key, message: user.errors.full_messages.first}
			    end
		    end
		    data
	    end

	    desc "Registers feeds"
	    params do
	    	requires :user_id,				type: Integer, 	desc: "User ID"
	    	requires :feeds_options,		type: String, 	desc: "Feeds Options Array String"
	    end

	    post :register_feeds do
	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		if user.feeds.present?
	    			feeds = user.feeds
	    			feeds.category_ids = params[:feeds_options]
	    		else
	    			feeds = user.build_feeds(
	    				category_ids: 						params[:feeds_options]
	    			)
	    		end
	    		if feeds.save
	    			data = {response: "success", status: "registered", message: "Feeds updated."}
	    		else
	    			key, val = user.errors.messages.first
					data = {response: "fail", status: key, message: user.errors.full_messages.first}
	    		end
	    	else
	    		data = {response: "fail", status: "no_exist_user", message: "Can't find this user"}
	    	end

	    	data
	    end

	    desc "Get feeds options"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end

	    get :feeds_options do
	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		if user.feeds.present?
	    			feeds_options = user.feeds_options
	    			data = { response: "success", status: "get_feeds", message: "Succeeded!", data: feeds_options }
	    		else
	    			data = { response: "fail", status: "no_exist_feeds", message: "No feeds!", data: "" }
	    		end
	    	else
	    		data = { response: "fail", status: "no_exist_user", message: "Can't find this user", data: "" }
	    	end

	    	data
	    end


	    desc "Get User's Topics"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end

	    get :feeds_topics do
	    	user = User.find_by_id(params[:user_id])
	    	page = params[:page].present? ? params[:page] :  1
	    	feeds_options = params[:feeds_options].present? ? params[:feeds_options] : ""
	    	
	    	if user.present?
	    		user_feeds_topics = user.feeds_topics(feeds_options).paginate(page: page)
	    		topics = user_feeds_topics.map{|topic| topic.api_detail(user)}
	    		data = { response: "success", status: "get_user_feeds_topics", message: "Succeeded!", data: topics, count: topics.count, page: page }
	    	else
	    		data = { response: "fail", status: "no_exist_user", message: "Can't find this user", data: "" }
	    	end

	    	data
	    end

	    desc "Get User's Self Topics"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end

	    get :self_topics do
	    	user = User.find_by_id(params[:user_id])
	    	page = params[:page].present? ? params[:page] :  1
	    	feeds_options = params[:feeds_options].present? ? params[:feeds_options] : ""
	    	
	    	if user.present?
	    		user_self_topics = user.self_topics(feeds_options).paginate(page: page)
	    		topics = user_self_topics.map{|topic| topic.api_detail(user)}
	    		data = { response: "success", status: "get_user_self_topics", message: "Succeeded!", data: topics, count: topics.count, page: page }
	    	else
	    		data = { response: "fail", status: "no_exist_user", message: "Can't find this user", data: "" }
	    	end

	    	data
	    end

	    desc "Register SignIn count"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    	requires :sign_in_count,		type: Integer, 	desc: "Signin Count"
	    end

	    post :register_sign_in_count do
	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		user.sign_in_count = params[:sign_in_count]
	    		user.password = ""
	    		if user.update_attributes(sign_in_count: params[:sign_in_count], password: "")
	    			data = {response: "success", status: "register_sign_in_count", messages: "Succeeded!"}
	    		else
	    			key, val = user.errors.messages.first
					data = {response: "fail", status: key, message: user.errors.full_messages.first}
	    		end
	    	else
	    		data = { response: "fail", status: "no_exist_user", message: "Can't find this user", data: "" }
	    	end

	    	data
	    end

	    desc "Update a user's details"
	    params do
	    	requires :username, 			type: String, 	desc: "martinek_123"
	    	requires :gender, 				type: Integer, 	desc: "1: male, 2: female"
	    	requires :dob, 					type: String, 	desc: "1982/01/05"
	    	requires :occupation,			type: String, 	desc: "Financial Advisor"
	    	requires :user_id, 				type: String, 	desc: "User ID"
	    	requires :photo_uuid, 			type: String, 	desc: "Photo UUID"
	    	requires :photo_changed, 		type: Boolean,  desc: "true: Photo File changed, false: Photo File not changed"
	    end
	    
	    post :update_user_details do
		    # user = User.where(first_name: params[:first_name], last_name: params[:last_name], specialty: params[:specialty]).first

		    p "REST API---------update_user-------------"
		    p "params : #{params}"

		    user = User.find_by_id(params[:user_id])

		    if user.present?
		    	dup_user = User.where("users.id<>'#{params[:user_id]}' and users.username='#{params[:username]}'").first

		    	if dup_user.present?
		    		data = {response: "fail", status: "duplicated_username", message: "Username already used."}
		    	else
		    		photo_changed = params[:photo_changed]
		    		photo_uuid = photo_changed ? params[:photo_uuid] : user.photo_uuid

		    		if user.update_attributes(	username: 	params[:username],
		    									birth: 		params[:dob],
		    									gender: 	params[:gender],
		    									occupation: params[:occupation],
		    									photo_uuid: photo_uuid,
		    									password: 	"")

			    		data = {response: "success", status: "registered", message: "User profile updated successfully."}

			    		if photo_changed
			    			p "Photo Changed------------will update the photo"

			    			UserPhoto.delay.transfer_and_cleanup(user.id)
			    		end
			    	else
			    		key, val = user.errors.messages.first
						data = {response: "fail", status: key, message: user.errors.full_messages.first}
			    	end
		    	end
		    else
		    	data = {response: "fail", status: "user_not_exist", message: "This user not exist."}
		    end
		    data
	    end

		desc "Update a user's email"
	    params do
	    	requires :email, 				type: String, 	desc: "User's email, martinek@email.com"
	    	requires :user_id, 				type: String, 	desc: "User ID"
	    end
	    
	    post :update_user_email do
		    # user = User.where(first_name: params[:first_name], last_name: params[:last_name], specialty: params[:specialty]).first

		    p "REST API---------update_user's email-------------"
		    p "params : #{params}"  

		    user = User.find_by_id(params[:user_id])

		    if user.present?
	    		dup_user = User.where("users.id<>'#{params[:user_id]}' and users.email='#{params[:email]}'").first
	    		if dup_user.present?
		    		data = {response: "fail", status: "duplicated_email", message: "Email already used."}
		    	else
		    		user.skip_confirmation!
		    		if user.update_attributes(	email: 		params[:email],
		    									password: 	"")

			    		data = {response: "success", status: "updated", message: "New email updated successfully."}
			    	else
			    		key, val = user.errors.messages.first
						data = {response: "fail", status: key, message: user.errors.full_messages.first}
			    	end
		    		# user.email = params[:email]
		    	end		    	
		    else
		    	data = {response: "fail", status: "user_not_exist", message: "This user not exist."}
		    end
		    data
	    end


	    desc "Update a user's password"
	    params do
	    	requires :old_password,			type: String, 	desc: "User's current password"
	    	requires :new_password,			type: String, 	desc: "User's old password"
	    	requires :user_id, 				type: String, 	desc: "User ID"
	    end
	    
	    post :update_user_password do
		    p "REST API---------update_user's password-------------"
		    p "params : #{params}"  

		    user = User.find_by_id(params[:user_id])

		    if user.present?
		    	if user.valid_password?(params[:old_password])
		    		user.skip_confirmation!
		    		if user.update_attributes(	password: 				params[:new_password],
		    									password_confirmation: 	params[:new_password])
			    		data = {response: "success", status: "updated", message: "New password updated successfully."}
			    	else
			    		key, val = user.errors.messages.first
						data = {response: "fail", status: key, message: user.errors.full_messages.first}
			    	end
			    else
			    	data = {response: "fail", status: "wrong_password", message: "Current password is incorrect"}
		    	end
		    else
		    	data = {response: "fail", status: "user_not_exist", message: "This user not exist."}
		    end
		    data
	    end

	    desc "Registers alert settings"
	    params do
	    	requires :user_id,				type: Integer, 	desc: "User ID"
	    	requires :alert_settings,		type: String, 	desc: "Alerts Settings Array String"
	    end

	    post :register_alert_settings do
	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		alert_settings_arr = params[:alert_settings].split(",")

	    		if alert_settings_arr.count != 4
	    			data = {response: "fail", status: "wrong_settings", message: "Wrong settings"}
	    		else
	    			if user.update_attributes(	alert_settings: 	params[:alert_settings],
		    									password: 			"")
			    		data = {response: "success", status: "updated", message: "Alert settings updated successfully."}
			    	else
			    		key, val = user.errors.messages.first
						data = {response: "fail", status: key, message: user.errors.full_messages.first}
			    	end
	    		end	    		
	    	else
	    		data = {response: "fail", status: "no_exist_user", message: "Can't find this user"}
	    	end

	    	data
	    end

	    desc "Get All Users"
	    params do
	    	requires :user_id, 				type: Integer,	desc: "User ID"
	    end

	    get :allusers do

	    	user = User.find_by_id(params[:user_id])
	    	if user.present?
	    		users = User.where.not(id: params[:user_id])
	    		# users = User.where("users.id<>'#{params[:user_id]}' and (users.first_name<>'' or users.last_name<>'')").all
	            res_users = users.map{|iuser| iuser.api_detail(user)}
	            data = { response: "success", status: "get_all_users", message: "Succeeded!", data: res_users, count: res_users.count}
	        else
	        	data = { response: "fail", status: "failed_find_user", message: "Can't find this user" }
	    	end
	    	
	    	data
	    end
  	end
end
class Dashboard::CategoriesController < DashboardController
	# respond_to :js, :only => [:create, :update, :destroy]
		
	def index
		p "dashboard categories index ---------------------------------"

		@categories = Category.all.order(:created_at)

		@categories = @categories.map{|category| category.info_detail}

		@breadcrumbs = [
	    	{label: "DASHBOARD", url: dashboard_path},
	    	{label: "CATEGORIES", url: dashboard_categories_path}
	    ]

	end

	def new

	end

	def create
		p "----------category create----------"
		p "params : #{params}, name : #{params[:category][:name]}, description : #{params[:category][:description]}"

		category = Category.new(category_params)

		if category.save
			@data = { success: true, message: "Category saved successfully." }
		else
			key, val = category.errors.messages.first
			@data = { success: false, message: category.errors.full_messages.first }
		end

		respond_to do |format|
			format.js
		end
		
		# render json: data
	end

	def update
		p "-----------category update----------"
	end

	def destroy
		p "-----------category delete----------"

		category_id = params[:id]

		category = Category.find(category_id)
		if category.present? && category.destroy
			data = { success: true, message: "Category destroyed successfully." }
		else
			data = { success: false, message: "Can't find this category" }
		end

		render json: data
	end

	private

  	def category_params
		p "-------category_params-------------"
		params.require(:category).permit(:name, :description, :image)
	end
end

class Dashboard::TopicsController < DashboardController

	def index
		p "dashboard topics index ---------------------------------"

		@topics = Topic.all

		@topics = @topics.map{|topic| topic.info_detail}

		@breadcrumbs = [
	    	{label: "DASHBOARD", url: dashboard_path},
	    	{label: "TOPICS", url: dashboard_topics_path}
	    ]

	end

	def destroy
		p "-----------topic delete----------"

		topic_id = params[:id]

		topic = Topic.find(topic_id)
		if topic.present? && topic.destroy
			data = { success: true, message: "Topic destroyed successfully." }
		else
			data = { success: false, message: "Can't find this topic" }
		end

		render json: data
	end
end

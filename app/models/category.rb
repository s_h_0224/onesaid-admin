class Category < ActiveRecord::Base
	self.inheritance_column = :_type_disabled

	has_attached_file :image,
						path:  "categories/#{Rails.env}/:attachment/:id/:style/:custom_filename", 
						styles: { thumb: "100*100>" }, 
						default_url: "missing.png"
	validates_attachment :image, 
						presence: true, 
						content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

	validates :name,  			presence: true
	validates_uniqueness_of :name

	has_many :topics, class_name: "Topic", foreign_key: "category_id",   dependent: :destroy

	def photo_url
		image.present? ? image.url : "missing.png"
	end

	def photo_thumb_url
		image.present? ? image.url(:thumb) : "missing.png"
	end

	def info_detail
	    {
	      id:               	id.to_s,
	      photo_url: 			photo_url,
	      photo_thumb_url: 		photo_thumb_url,
	      name:        			name,
	      description:      	description,
	      topics_count:     	topics.count
	    }
	end

	def api_detail
    {
      id:               	id.to_s,
      name:            		name,
      description:      	description,
      photo_url: 			photo_url,
      photo_thumb_url: 		photo_thumb_url,
      topics_count: 		topics.count,
    }
  end

end

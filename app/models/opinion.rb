class Opinion < ActiveRecord::Base

	belongs_to :topic,				class_name: "Topic"
	belongs_to :user,				class_name: "User"

	def info_detail
    {
		id:               		id.to_s,
		score:        			score.to_s,
		comment: 				comment,
		tags: 					tags,
		topic_id: 				topic_id,
		topic_name: 			topic.name,
		user_id: 				user_id,
		user_name: 				user.full_name
    }
	end

	def api_detail
    {
		id:               		id.to_s,
		score:        			score.to_s,
		comment: 				comment,
		tags: 					tags,
		topic_id: 				topic_id,
		topic_name: 			topic.name,
		user_id: 				user_id,
		user_name: 				user.full_name,
		created_time:     		created_at.strftime("%Y-%m-%d %H:%M:%S"),
    }
  	end

end

class Topic < ActiveRecord::Base
	validates :name,  			presence: true
	# validates_uniqueness_of :name

	belongs_to :category,			class_name: "Category"
	belongs_to :user,				class_name: "User"

	default_scope { order(created_at: 'DESC') }

	scope :from_users_followed_by, lambda {|user, feeds_options| followed_by(user, feeds_options)}
	
	has_many :opinions, class_name: "Opinion", foreign_key: "topic_id",   dependent: :destroy

	has_one :topic_photo, class_name: "TopicPhoto", foreign_key: "topic_id",   dependent: :destroy

	def photo_url
		topic_photo.present? ? topic_photo.photo_url : "missing.png"
	end

	def photo_thumb_url
		topic_photo.present? ? topic_photo.photo_thumb_url : "missing.png"
	end

	# def self.from_users_followed_by(user)
	# 	followed_ids = user.following.map(&:id).join(", ")
	# 	where("user_id IN (#{followed_ids}) OR user_id = ?", user)
	# end

	def info_detail
    {
		id:               		id.to_s,
		name:        			name,
		description: 			description,
		photo_url: 				photo_url,
		photo_thumb_url: 		photo_thumb_url,
		category_id: 			category_id,
		category_name: 			category.name,
		user_id: 				user_id,
		user_name: 				user.username,
		user_fullname: 			user.full_name,
		user_email: 			user.email,
		opinions_count: 		opinions.count
    }
	end

	def api_detail(user1)
    {
		id:               		id.to_s,
		name:            		name,
		description: 			description,
		photo_url: 				photo_url,
		photo_thumb_url: 		photo_thumb_url,
		category_id: 			category_id,
		category_name: 			category.name,
		opinion_count: 			10,
		created_time:     		created_at.strftime("%Y-%m-%d %H:%M:%S"),
		user: 					user.api_detail(user1)
    }
  end

  private
  	def self.followed_by(user, feeds_options)
  		followed_ids = user.following.map(&:id).join(", ")
  		followed_ids = %(SELECT followed_id FROM relationships WHERE follower_id = :user_id)
  		if feeds_options.empty?
  			where("user_id IN (#{followed_ids}) OR user_id = :user_id", {:user_id => user})
  		else
  			where("(user_id IN (#{followed_ids}) OR user_id = :user_id) AND category_id IN (#{feeds_options})", {:user_id => user})
  		end
		
  	end
end

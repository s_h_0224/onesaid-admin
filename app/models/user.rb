class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :confirmable,
         :recoverable, :rememberable, :trackable, :validatable

  # Constants
  #----------------------------------------------------------------------
  USER_ROLE = {admin: 1, app: 2}
  USER_TYPE = {email: 1, fb: 2}
  USER_TYPE_LABEL = ["", "email", "fb"]

  # validates :first_name,  	 presence: true
  # validates :last_name,   	 presence: true
  validates :email,   		   presence: true
  validates :username,       presence: true
  validates :user_type,      :numericality => {:greater_than => 0}, :presence => true
  validates_uniqueness_of :email, case_sensitive: false

  has_many :opinions,     class_name: "Opinion",    foreign_key: "user_id",       dependent: :destroy
  has_many :topics,       class_name: "Topic",      foreign_key: "user_id",       dependent: :destroy

  has_one  :feeds,        class_name: "Feed",       foreign_key: "user_id",       dependent: :destroy
  has_one  :friends,      class_name: "Friend",     foreign_key: "user_id",       dependent: :destroy

  has_many :relationships, class_name: "Relationship", foreign_key: "follower_id",   dependent: :destroy
  has_many :following, through: :relationships,     source: :followed

  has_many :reverse_relationships, class_name: "Relationship", foreign_key: "followed_id", dependent: :destroy
  has_many :followers, through: :reverse_relationships, source: :follower

  has_one :user_photo,    class_name: "UserPhoto",  foreign_key: "user_id",       dependent: :destroy

  # Overrides Devise methods
  #----------------------------------------------------------------------
  def password_required?
    user_role == USER_ROLE[:admin] ? super : !super
  end

  # Validation methods
  #----------------------------------------------------------------------
  # Checks user is admin user
  def admin_user?
    user_role == USER_ROLE[:admin]
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  def feeds_options
    feeds.present? ? feeds.category_ids : ""
  end

  def feeds_topics(feeds_options)
    Topic.from_users_followed_by(self, feeds_options)
  end

  def self_topics(feeds_options)
    if feeds_options.empty?
      topics
    else
      topics.where("category_id IN (#{feeds_options})")
    end
  end

  def friend_ids
    friends.present? ? friends.friend_ids : ""
  end

  def friends_list
    friends_arr = []
    if friends.present?
      # friends = User.in(id: friend_ids.split(","))
      friend_ids.split(",").each do |friend_id|
        friends_arr << User.where(id: friend_id).first
      end
    end
    friends_arr
  end

  def add_friend(friend)
    user        =   self

    if is_friend(friend)
      true
    else
      if friends.present?
        f_ids_arr = friend_ids.split(",") << friend.id.to_s
        friends = user.friends
        friends.friend_ids = f_ids_arr.join(",")
      else
        friends = user.build_friends(
          friend_ids:         friend.id.to_s
        )
      end

      if friends.save
        true
      else
        false
      end
    end    
  end

  def is_friend(friend)
    friends.present? ? friend_ids.split(",").include?(friend.id.to_s) : false
  end

 # Following part
  def following?(followed)
    relationships.find_by_followed_id(followed).present?
  end

  def follow!(followed)
    if !following?(followed)
      relationships.create!(followed_id: followed.id)
    end
  end

  def unfollow!(followed)
    if relationships.find_by_followed_id(followed).present?
      relationships.find_by_followed_id(followed).destroy
    end
  end

  def photo_url
    user_photo.present? ? user_photo.photo_url : "missing.png"
  end

  def photo_thumb_url
    user_photo.present? ? user_photo.photo_thumb_url : "missing.png"
  end

  def api_detail(user1)
    {
      id:               id.to_s,
      email:            email,
      username:         username,
      gender:           gender,
      birth:            birth.present? ? birth.strftime("%Y-%m-%d %H:%M:%S") : "1900-01-01 00:00:00",
      occupation:       occupation,
      first_name:       first_name,
      last_name:        last_name,
      member_since:     created_at.strftime("%Y"),
      created_time:     created_at.strftime("%Y-%m-%d %H:%M:%S"),
      # user_role:        user_role,
      user_type:        user_type,
      sign_in_count:    sign_in_count,
      feeds_options:    feeds_options,
      photo_url:        photo_url,
      photo_thumb_url:  photo_thumb_url,
      alert_settings:   alert_settings,
      opinions_count:   1100,
      comments_count:   1200,
      echos_count:      1300,
      added_count:      1400,
      following_count:  following.count,
      followers_count:  followers.count,
      is_following:     user1.present? ? user1.following?(self) : false,
      is_follower:      user1.present? ? following?(user1) : false
    }
  end

  def user_info_detail
    {
      id:               id.to_s,
      full_name:        full_name,
      username:         username,
      email:            email,
      member_since:     created_at.strftime("%Y"),
      sign_in_count:    sign_in_count.to_s,
      user_type:        USER_TYPE_LABEL[user_type],
      photo_url:        photo_url,
      photo_thumb_url:  photo_thumb_url,
      is_admin:         admin_user? ? "Yes" : "No"
    }
  end
end

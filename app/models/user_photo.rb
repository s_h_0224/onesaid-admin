class UserPhoto < ActiveRecord::Base
	has_attached_file :image,
						path:  "users/#{Rails.env}/:attachment/:id/:style/:custom_filename", 
						styles: { thumb: "100*100>" }, 
						default_url: "missing.png"
	validates_attachment :image, 
						presence: true, 
						content_type: { content_type: ["image/jpeg", "image/gif", "image/png"] }

	belongs_to :user,				class_name: "User"

	def photo_url
		image.present? ? image.url : ""
	end

	def photo_thumb_url
	 	image.present? ? image.url(:thumb) : ""
	end

	# Final upload processing step
	def self.transfer_and_cleanup(user_id)
  		p "------------transfer_and_cleanup-----------user_id :#{user_id}"

  		user = User.find_by_id(user_id)
  		if user.present?
	  		s3 = AWS::S3.new
		    direct_uploaded_original_file_path 	= "temp/#{user.photo_uuid}.jpg"
		    direct_uploaded_thumb_file_path 	= "temp/#{user.photo_uuid}_thumb.jpg"

		    # Copies image data info
		    direct_upload_original_head = s3.buckets[ENV['S3_API_BUCKET']].objects[direct_uploaded_original_file_path].head
		    direct_upload_thumb_head 	= s3.buckets[ENV['S3_API_BUCKET']].objects[direct_uploaded_thumb_file_path].head

		    photo = user.create_user_photo(image_file_name: "#{user.photo_uuid}.jpg",
		                         image_file_size: direct_upload_original_head.content_length,
		                         image_content_type: direct_upload_original_head.content_type,
		                         image_updated_at: direct_upload_original_head.last_modified)

		    p "image file name : #{user.photo_uuid}.jpg"
		    p "image content type : #{direct_upload_original_head.content_type}"
		    p "----error ------#{photo.errors.full_messages}"

		    # Moves direct uploaded file to the correct path on S3
		    paperclip_file_original_path 	= "users/#{Rails.env}/images/#{photo.id}/original/#{user.photo_uuid}.jpg"
		    paperclip_file_thumb_path 		= "users/#{Rails.env}/images/#{photo.id}/thumb/#{user.photo_uuid}.jpg"
		    
		    s3.buckets[ENV['S3_API_BUCKET']].objects[paperclip_file_original_path].copy_from(direct_uploaded_original_file_path)
		    s3.buckets[ENV['S3_API_BUCKET']].objects[paperclip_file_original_path].acl = :public_read

		    s3.buckets[ENV['S3_API_BUCKET']].objects[paperclip_file_thumb_path].copy_from(direct_uploaded_thumb_file_path)
		    s3.buckets[ENV['S3_API_BUCKET']].objects[paperclip_file_thumb_path].acl = :public_read

		    # Deletes the direct uploaded file
		    s3.buckets[ENV['S3_API_BUCKET']].objects[direct_uploaded_original_file_path].delete
		    s3.buckets[ENV['S3_API_BUCKET']].objects[direct_uploaded_thumb_file_path].delete
  		end
  	end
end
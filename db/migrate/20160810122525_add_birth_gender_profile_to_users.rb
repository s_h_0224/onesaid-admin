class AddBirthGenderProfileToUsers < ActiveRecord::Migration
  def change
    add_column :users, :birth, :datetime
    add_column :users, :gender, :integer
    add_column :users, :profile_photo, :string
  end
end

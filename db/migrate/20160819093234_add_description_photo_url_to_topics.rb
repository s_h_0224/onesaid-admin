class AddDescriptionPhotoUrlToTopics < ActiveRecord::Migration
  def change
    add_column :topics, :description, :string
    add_column :topics, :photo_url, :string
  end
end

class RemoveAttachFromTopic < ActiveRecord::Migration
  def change
  	remove_column :topics, :image_file_name
  	remove_column :topics, :image_content_type
  	remove_column :topics, :image_file_size
  	remove_column :topics, :image_updated_at
  end
end

class CreateTopicPhotos < ActiveRecord::Migration
  def change
    create_table :topic_photos do |t|
      t.integer :topic_id

      t.timestamps null: false
    end
  end
end

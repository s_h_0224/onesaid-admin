class AddAttachmentImageToTopicPhotos < ActiveRecord::Migration
  def self.up
    change_table :topic_photos do |t|
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :topic_photos, :image
  end
end

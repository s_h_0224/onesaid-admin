class CreateFeeds < ActiveRecord::Migration
  def change
    create_table :feeds do |t|
    	t.string :category_ids, 	null: false, default: ""
    	t.integer :user_id, 			null: false

      t.timestamps null: false
    end
  end
end

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

p "- Destroy User"
User.destroy_all

p "- Create Admin for OneSaid"
user = User.where(email: "admin@onesaid.com").first
unless user.present?
  admin = User.create(
  			email: 						            "admin@onesaid.com", 
        username:                     "admin_onesaid",
  			password: 					          "admin321",
    		password_confirmation: 		    "admin321", 
        birth:                        "1982-01-05",
        gender:                       1,
    		user_role: 				            User::USER_ROLE[:admin],
        user_type:                    User::USER_TYPE[:email],
    		first_name: 				          "Adrian", 
    		last_name: 					          "Martinek", 
    		confirmed_at: 				        Time.now, 
    		status: 					            1, 
    		registered_at: 				        Time.now)
  
  admin = User.find(admin.id.to_s)
  admin.confirm!
end